CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Installation
 * Maintainers

INTRODUCTION
------------
EditHub provides a fully configurable, discrete and easy to use administration
menu.
Each feature is displayed in a section and has a set of actions. Each action is
displayed according to the current user's permissions.
Sections, features and actions are configurable with the hook_edithub and
editable with hook_edithub_alter

INSTALLATION
------------
* Install as usual, for further information see
  https://drupal.org/documentation/install/modules-themes/modules-7
* Add $edit_hub variable to your html.tpl.php template file.
* Implement hook_edithub_links() to add links as described in edithub.api.php

MAINTAINERS
-----------
Current maintainers:
 * Raphael Khaiat (bacardi55) - https://drupal.org/user/2830515
 * Julien Decaudin (Juliuss77) - https://drupal.org/user/540900
 * Philippe Mouchel (OwilliwO) - https://drupal.org/user/2393662

This project has been sponsored by:
 * RADIO FRANCE
  French national radio group
