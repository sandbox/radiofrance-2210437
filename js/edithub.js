/**
 * @file
 * Client behaviors.
 */

(function ($) {

  // Animation de l'edit hub.
  Drupal.behaviors.editHub = {
    attach: function(context, settings) {
      var editHub = $('#edit-hub', context),
          editHubOpener = $('span.edit-hub-button', editHub),
          editHubEntries = $('ul.entries', editHub),
          editHubEntriesOpeners = editHubEntries.find('> li.entry > span');

      // Ouverture / Fermeture de l'edit hub.
      editHubOpener.click(function() {

        // Ouverture.
        if (editHub.hasClass('closed')) {
          editHub.children('.wrapped').animate({width:'198px'}, 'fast', 'linear', function() {
            editHub.removeClass('closed');
          });
        }

        // Fermeture.
        else {
          editHub.children('.wrapped').animate({width:'0'}, 'fast', 'linear', function() {
            editHub.addClass('closed');
          });
        }
      });

      // Ouverture / Fermeture d'une entrée.
      editHubEntriesOpeners.click(function() {
        var thisParent = $(this).parent(),
            thisSubEntry = $(this).siblings('ul.subentries'),
            openParents = thisParent.siblings(':not(.closed)');

        // Ouverture.
        if (thisParent.hasClass('closed')) {
          thisSubEntry.slideDown('fast', function() {
            thisParent.removeClass('closed');
          });
          openParents.children('ul.subentries').slideUp('fast', function() {
            $(this).parent().addClass('closed');
          });
        }

        // // Fermeture.
        else {
          thisSubEntry.slideUp('fast', function() {
            thisParent.addClass('closed');
          });
        }
      });
    }
  }

})(jQuery);
