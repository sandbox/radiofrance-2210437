<?php
/**
 * @file
 * EditHub button template.
 */
?>
<li class="button <?php print $button; ?>">
  <?php print l($button, $url); ?>
</li>
