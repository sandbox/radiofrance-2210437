<?php
/**
 * @file
 * EditHub subentry template.
 */
?>
<li class="subentry">
  <p class="label"><?php print $label; ?></p>
  <?php if ($buttons): ?>
  <ul class="buttons">
    <?php print $buttons; ?>
  </ul>
  <?php endif; ?>
</li>
