<?php
/**
 * @file
 * EditHub entry template.
 */
?>
<li class="entry closed">
  <p class="label"><?php print $label; ?></p>
  <?php if ($subentries): ?>
    <span class="subentries-opener closed">Open</span>
    <ul class="subentries">
      <?php print $subentries; ?>
    </ul>
  <?php endif; ?>
</li>
