<?php
/**
 * @file
 * EditHub wrapper template.
 */
?>
<div id="edit-hub" class="closed">
  <div class="wrapped">
    <span class="edit-hub-button">Open</span>
    <?php if ($entries): ?>
    <ul class="entries">
      <?php print $entries; ?>
    </ul>
    <?php endif; ?>
  </div>
</div>
