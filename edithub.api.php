<?php
/**
 * @file
 * Hooks provided by edithub API.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Add links to the edithub links.
 *
 * @return array
 *   An array of containers and links:
 *   - "label": Required. The label for the container.
 *   - "weight": Optionnal. The weight
 *   - "entries": Required. An array of entry inside that container.
 *     - "label":
 *     - 'links': An array of links / button for this entry:
 *       - "button": label of the button. Possible values are:
 *         "order", "add", "list", "config", "logout".
 *       - "url": the url of the button.
 *       - "permission": The needed permission.
 *       - "weight": The weight.
 */
function hook_edithub_links() {
  $items = array();

  // Container.
  $items['myContainer']['label'] = t('Highlights');
  $items['myContainer']['weight'] = 0;
  $items['myContainer']['entries']['myAction'] = array(
    'label' => t('The beautiful title'),
    'links' => array(
      array(
        // Button type.
        'button' => 'order',
        // The url.
        'url' => '',
        'permission' => '',
        'weight' => 0,
      ),
    ),
  );

  return $items;
}

/**
 * Alter links from edithub.
 *
 * @param array $items
 *   Edithub items.
 *
 * @see hook_edithub_links()
 */
function hook_edithub_links_alter(array &$items) {
  $items['myContainer']['entries']['myAction']['permission'] = 'changed permission';
}

/**
 * @} End of "addtogroup hooks".
 */
