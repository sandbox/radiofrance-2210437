<?php
/**
 * @file
 * Function around html rendering.
 */

/**
 * Get cached rendered HTML for edithub, based on the user.
 *
 * @param object $user
 *   Standard user object.
 *
 * @return string
 *   Rendered HTML
 */
function edithub_get_html($user) {
  $cid = 'edithub:' . $user->uid;

  // Get rendered HTML from cache table.
  if ($cache_object = cache_get($cid)) {
    $cache = $cache_object->data;
  }
  // If not exists, store rendered HTML in cache table.
  else {
    $cache = theme('edithub', $user);
    cache_set($cid, $cache);
  }

  // Return rendered HTML.
  return $cache;
}

/**
 * Render HTML for edithub, based on the user.
 *
 * @param object $user_in
 *   Optional - standard user object.
 *
 * @return string
 *   Rendered HTML
 */
function theme_edithub($user_in = NULL) {
  global $user;

  // TODO: Add possibility to override the default view.
  $user_in = (!empty($user_in) && !empty($user_in['user'])) ? $user_in : $user;

  // Ask all modules to give them items for edithub.
  $items = array();
  $items = module_invoke_all('edithub_links');
  drupal_alter('edithub_links', $items);

  $html_entries = '';
  foreach (_edithub_order_submenu($items) as $item) {
    $html_subentries = '';
    if (isset($item['entries'])) {
      foreach (_edithub_order_submenu($item['entries']) as $entry) {
        $html_buttons = '';
        foreach (_edithub_order_submenu($entry['links']) as $link) {
          // Transform taxonomy permission,
          // must get vid (instead if machine_name) to check permission.
          if (strpos($link['permission'], 'edit terms in ') !== FALSE) {
            $name = str_replace('edit terms in ', '', $link['permission']);
            $vocab = taxonomy_vocabulary_machine_name_load($name);
            $link['permission'] = str_replace(
              $name,
              $vocab->vid,
              $link['permission']
            );
          }

          // No permission needed.
          if (empty($link['permission'])) {
            $html_buttons .= theme('edithub_button', $link);
          }

          // Check needed permission.
          elseif (user_access($link['permission'], $user_in)) {
            $html_buttons .= theme('edithub_button', $link);
          }
        }

        // Render buttons list.
        if (!empty($html_buttons)) {
          $html_subentries .= theme('edithub_subentry', array(
            'label' => $entry['label'],
            'buttons' => $html_buttons,
          ));
        }
      }
    }

    // Render subentries list.
    if (!empty($html_subentries)) {
      $html_entries .= theme('edithub_entry', array(
        'label' => $item['label'],
        'subentries' => $html_subentries,
      ));
    }
  }

  // Return rendered list (if not empty).
  if (!empty($html_entries)) {
    return theme('edithub_wrapper', array('entries' => $html_entries));
  }
  else {
    return '';
  }
}

/**
 * Re order the submenu according to the weight.
 *
 * @param array $entry
 *   Array of ordered entries.
 *
 * @return array
 *   Ordered entries
 */
function _edithub_order_submenu(array $entry = array()) {
  $callback = function ($a, $b) {
    $wa = isset($a['weight']) ? $a['weight'] : 0;
    $wb = isset($b['weight']) ? $b['weight'] : 0;

    if ($wa == $wb) {
      return 0;
    }

    return $wa - $wb;
  };
  usort($entry, $callback);

  return $entry;
}
