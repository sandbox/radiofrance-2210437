<?php
/**
 * @file
 * Module file, Hooks implementations.
 */

/**
 * Implements hook_views_api().
 */
function edithub_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_permission().
 */
function edithub_permission() {
  $perms = array();

  // Permission to display Edithub.
  $perms['access edithub'] = array(
    'title' => t('Access edithub'),
    'description' => t('Allow user to navigate through edithub.'),
  );

  // Permission to list content.
  $perms['access edithub content lists'] = array(
    'title' => t('Access edithub content lists'),
    'description' => t('Allow user to list content in backoffice.'),
  );

  return $perms;
}

/**
 * Implements hook_theme().
 */
function edithub_theme($existing, $type, $theme, $path) {
  $themes = array();

  // Global theme.
  $themes['edithub_wrapper'] = array(
    'template' => 'templates/edithub-wrapper',
    'variables' => array(
      'entries' => NULL,
    ),
  );

  // Edithub theme.
  $themes['edithub'] = array(
    'variables' => array(
      'user' => NULL,
    ),
    'file' => 'edithub.html.inc',
  );

  // Entry theme.
  $themes['edithub_entry'] = array(
    'template' => 'templates/edithub-entry',
    'variables' => array(
      'label' => NULL,
      'subentries' => NULL,
    ),
  );

  // Subentry theme.
  $themes['edithub_subentry'] = array(
    'template' => 'templates/edithub-subentry',
    'variables' => array(
      'label' => NULL,
      'buttons' => NULL,
    ),
  );

  // Button theme.
  $themes['edithub_button'] = array(
    'template' => 'templates/edithub-button',
    'variables' => array(
      'button' => NULL,
      'url' => NULL,
    ),
  );

  return $themes;
}

/**
 * Implements hook_preprocess_html().
 */
function edithub_preprocess_html(&$vars) {
  // Nothing to do for unauthorized users.
  if (!user_access('access edithub')) {
    return;
  }

  // Module path.
  $module_path = drupal_get_path('module', 'edithub');

  // CSS attachment.
  drupal_add_css($module_path . '/css/edithub.css');

  // JS attachment.
  drupal_add_js($module_path . '/js/edithub.js');

  // HTML class for body.
  $vars['classes_array'][] = 'edit-hub';

  // Rendered HTML.
  $vars['page']['page_bottom']['edithub'] = array(
    '#type'   => 'markup',
    '#markup' => theme('edithub'),
  );
}
